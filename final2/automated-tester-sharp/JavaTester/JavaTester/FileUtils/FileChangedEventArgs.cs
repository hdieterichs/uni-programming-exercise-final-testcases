/**
 * Author: Anton Grawitter
 */
namespace JavaTester.File {
    public class FileChangedEventArgs {
        public FileChangedEventArgs(FileDependable file) {
            File = file;
        }
        public FileDependable File { get; private set; }
    }
}