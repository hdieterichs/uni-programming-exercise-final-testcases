﻿namespace JavaTester.File {
    using System;
    using System.Diagnostics;
    using System.IO;
    public sealed class TemporaryFolder : IDisposable {
        public TemporaryFolder(string folderName = null) {
            string folder = Path.Combine(Path.GetTempPath(), folderName ?? Process.GetCurrentProcess().ProcessName + "-" + Process.GetCurrentProcess().Id + "-" + Guid.NewGuid());
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            DirectoryPath = folder;
        }
        public bool IsDisposed { get; private set; }
        public String DirectoryPath { get; private set; }
        public void Dispose() {
            CleanUp();
            GC.SuppressFinalize(this);
        }
        private void CleanUp() {
            if (IsDisposed) return;
            IsDisposed = true;
            try {
                if (DirectoryPath != null && Directory.Exists(DirectoryPath))
                    Directory.Delete(DirectoryPath, true);
            } catch {
            } finally {
                DirectoryPath = null;
            }
        }
        ~TemporaryFolder() {
            CleanUp();
        }
    }
}