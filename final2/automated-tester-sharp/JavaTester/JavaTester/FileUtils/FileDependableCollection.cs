﻿/**
 * Author: Anton Grawitter
 * Collection that holds list of a file dependant class that synchronizes wich is always synchronized with the files on the file system
 * 
 * * Remarks: *
 *  Generic instantiation, Invokation of main dispatcher, Property Notification, Resorting everyting on add etc...
 *  Obviously makes everything a little slow, but it gets the job done.
 */
namespace JavaTester.File {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Annotations;
    public sealed class FileDependableCollection<T> : IEnumerable<T>, ICollection, INotifyCollectionChanged, INotifyPropertyChanged, IDisposable where T : FileDependable, new() {
        public delegate void FileChangedHandler(Object sender, FileChangedEventArgs args);
        private readonly Collection<T> _files = new Collection<T>();
        private readonly TaskFactory _tasks = new TaskFactory(TaskScheduler.FromCurrentSynchronizationContext());
        private readonly object _threadLock = new object();
        private FileSystemWatcher _fileWatcher = new FileSystemWatcher();
        private string _filter;
        private bool _keepSorted;
        public FileDependableCollection(String directoryPath, string filter = null, bool includeSubDirectories = false) {
            if (!Directory.Exists(directoryPath))
                throw new DirectoryNotFoundException("Directory " + directoryPath + "is invalid.");
            DirectoryPath = Path.GetFullPath(directoryPath);
            Filter = filter ?? "*.*";
            try {
                _fileWatcher = new FileSystemWatcher {
                    Path = DirectoryPath,
                    Filter = "*.*",
                    IncludeSubdirectories = includeSubDirectories,
                    EnableRaisingEvents = true,
                };
                _fileWatcher.Changed += OnFileChanged;
                _fileWatcher.Created += OnFileChanged;
                _fileWatcher.Deleted += OnFileChanged;
                _fileWatcher.Renamed += OnFileChanged;
            } catch (Exception e) {
                throw new DirectoryNotFoundException("File Deamon for " + directoryPath + " could not be started", e);
            }
        }
        [NotNull]
        public string DirectoryPath { get; private set; }
        public bool IsDisposed { get; private set; }
        public string Filter {
            get { return _filter; }
            set {
                if (_filter == value) return;
                _filter = value;
                DoFullFileSearch();
            }
        }
        public bool KeepSorted {
            get { return _keepSorted; }
            set {
                if (_keepSorted == value) return;
                _keepSorted = value;
                DoFullFileSearch();
            }
        }
        public void CopyTo(Array array, int index) {
            _tasks.StartNew(() => {
                if (array is T[])
                    _files.CopyTo(array as T[], index);
            });
        }
        public int Count {
            get { return _files.Count; }
        }
        public object SyncRoot { get; private set; }
        public bool IsSynchronized { get; private set; }
        public void Dispose() {
            if (IsDisposed) return;
            IsDisposed = true;
            if (_fileWatcher != null) {
                _fileWatcher.EnableRaisingEvents = false;
                _fileWatcher.Dispose();
                _fileWatcher = null;
            }
            GC.SuppressFinalize(this);
        }
        public IEnumerator<T> GetEnumerator() {
            return _files.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        private static bool PathEquals(string source, string comparison) {
            source = Path.GetFullPath(new Uri(source).LocalPath).TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            comparison = Path.GetFullPath(new Uri(comparison).LocalPath).TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            return String.Equals(source, comparison, StringComparison.OrdinalIgnoreCase);
        }
        private bool IsFilterMatch(string filePath) {
            if (Filter == null) return true;
            string fileName = Path.GetFileName(filePath);
            if (fileName == null) return false;
            try {
                var mask = new Regex(Filter.Replace(".", "[.]").Replace("*", ".*").Replace("?", "."));
                return mask.IsMatch(fileName);
            } catch {
                return false;
            }
        }
        public event FileChangedHandler FileAdded;
        private void OnFileAdded(FileChangedEventArgs args) {
            FileChangedHandler handler = FileAdded;
            if (handler != null) handler(this, args);
        }
        public event FileChangedHandler FileRemoved;
        private void OnFileRemoved(FileChangedEventArgs args) {
            FileChangedHandler handler = FileRemoved;
            if (handler != null) handler(this, args);
        }
        private void DoFullFileSearch() {
            _files.Clear();
            foreach (T file in Directory.GetFiles(DirectoryPath).Select(GetFileFromPath).Where(file => file != null)) {
                _files.Add(file);
                OnFileAdded(new FileChangedEventArgs(file));
            }
            if (KeepSorted)
                Sort();
            if (CollectionChanged != null)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        private T GetFileFromPath(string filePath) {
            if (!IsFilterMatch(filePath))
                return null;
            return new T {FilePath = filePath};
        }
        private bool AddFileAndNotify(string filePath) {
            T file = GetFileFromPath(filePath);
            if (file != null)
                Add(file);
            else
                return false;
            OnFileAdded(new FileChangedEventArgs(file));
            return true;
        }
        private bool RemoveFileAndNotify(string filePath) {
            if (!IsFilterMatch(filePath))
                return false;
            bool hasRemoved = false;
            _files.Where(o => PathEquals(filePath, o.FilePath)).ToList().ForEach(o => {
                Remove(o);
                OnFileRemoved(new FileChangedEventArgs(o));
                hasRemoved = true;
            });
            return hasRemoved;
        }
        private void OnFileChanged(object source, FileSystemEventArgs e) {
            new Action(() => {
                //We dont want to stop the io thread, or we wont get notified when alot of files are changed
                lock(_threadLock) {
                    //lock it immediatly, so we dont run into mt problems
                    switch (e.ChangeType) {
                        case WatcherChangeTypes.Renamed:
                            if (e is RenamedEventArgs)
                                RemoveFileAndNotify((e as RenamedEventArgs).OldFullPath);
                            AddFileAndNotify(e.FullPath);
                            break;
                        case WatcherChangeTypes.Changed:
                            if (RemoveFileAndNotify(e.FullPath))
                                AddFileAndNotify(e.FullPath);
                            break;
                        case WatcherChangeTypes.Created:
                            AddFileAndNotify(e.FullPath);
                            break;
                        case WatcherChangeTypes.Deleted:
                            RemoveFileAndNotify(e.FullPath);
                            break;
                    }
                }
            }).BeginInvoke(null, null);
        }
        private void Sort() {
            List<T> sortedList = _files.OrderBy(o => o).ToList();
            _files.Clear();
            foreach (T sortedItem in sortedList)
                _files.Add(sortedItem);
        }
        private void Add(T item) {
            _tasks.StartNew(() => {
                _files.Add(item);
                if (KeepSorted) {
                    Sort();
                    if (CollectionChanged != null)
                        CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                } else if (CollectionChanged != null)
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
            });
        }
        private void Clear() {
            _tasks.StartNew(() => {
                _files.Clear();
                if (CollectionChanged != null)
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            });
        }
        private bool Remove(T item) {
            return _tasks.StartNew(() => {
                int index = _files.IndexOf(item);
                if (index == -1) return false;
                if (!_files.Remove(item)) return false;
                if (KeepSorted) {
                    Sort();
                    if (CollectionChanged != null)
                        CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                } else if (CollectionChanged != null)
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                return true;
            }).Result;
        }
        ~FileDependableCollection() {
            Dispose();
        }
    }
}