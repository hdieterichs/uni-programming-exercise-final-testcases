﻿/**
 * Author: Anton Grawitter
 * 
 * Temporary file that is cleaned up automatically
 */
namespace JavaTester.File {
    using System;
    using System.Diagnostics;
    using System.IO;
    public sealed class TemporaryFile : IDisposable {
        public TemporaryFile(string fileName = null) {
            string folder = Path.Combine(Path.GetTempPath(), Process.GetCurrentProcess().ProcessName + "-" + Process.GetCurrentProcess().Id);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            FilePath = Path.Combine(folder, fileName ?? Guid.NewGuid() + ".temp");
        }
        public bool IsDisposed { get; private set; }
        public String FilePath { get; private set; }
        public byte[] Data {
            get { return File.Exists(FilePath) ? File.ReadAllBytes(FilePath) : null; }
            set {
                if (value == null) {
                    if (File.Exists(FilePath))
                        File.Delete(FilePath);
                } else
                    File.WriteAllBytes(FilePath, value);
            }
        }
        public void Dispose() {
            CleanUp();
            GC.SuppressFinalize(this);
        }
        private void CleanUp() {
            if (IsDisposed) return;
            IsDisposed = true;
            try {
                if (File.Exists(FilePath))
                    File.Delete(FilePath);
                string path = Path.GetDirectoryName(FilePath);
                if (path == null || !Directory.Exists(path)) return;
                if (Directory.GetFiles(path).Length == 0) Directory.Delete(path);
            } catch {
            } finally {
                FilePath = null;
            }
        }
        ~TemporaryFile() {
            CleanUp();
        }
    }
}