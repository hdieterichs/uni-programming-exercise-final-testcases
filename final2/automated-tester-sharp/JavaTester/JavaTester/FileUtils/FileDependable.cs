﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.File {
    using System;
    using System.ComponentModel;
    using System.IO;
    using Annotations;
    public abstract class FileDependable : INotifyPropertyChanged, IComparable<FileDependable> {
        [CanBeNull] private string _filePath;
        public string FilePath {
            get { return _filePath; }
            set {
                if (_filePath == value) return;
                if (!File.Exists(value))
                    throw new ArgumentException("File " + value + " not found!");
                _filePath = Path.GetFullPath(value);
                Init(FilePath);
                OnPropertyChanged("FilePath");
            }
        }
        public int CompareTo(FileDependable other) {
            return String.Compare(FilePath, other.FilePath, StringComparison.OrdinalIgnoreCase);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected abstract void Init(string path);
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}