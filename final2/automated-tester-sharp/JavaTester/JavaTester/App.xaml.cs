﻿namespace JavaTester {
    using System;
    using System.Globalization;
    using System.Threading;
    using System.Windows;
    using Java;
    /// <summary>
    ///     Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application {
        protected override void OnStartup(StartupEventArgs e) {
            if (!JavaExecutor.IsInstalled) {
                if (MessageBox.Show("Java not installed. Open Download Page?", "Java Installation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    JavaExecutor.OpenUpdate();
                Environment.Exit(0);
            }
            base.OnStartup(e);
            DispatcherUnhandledException += (sender, args) => {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                MessageBox.Show(String.Format("{0}:\n\n{1}\n\n{2}", args.Exception.Message, GetExceptionMessages(args.Exception.InnerException), args.Exception),
                    "Uncaught Exception - " + args.Exception.Message,
                    MessageBoxButton.OK);
                args.Handled = true;
                Environment.Exit(1);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) => {
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");
                var ex = (Exception)args.ExceptionObject;
                MessageBox.Show(String.Format("{0}:\n\n{1}\n\n{2}", ex.Message, GetExceptionMessages(ex.InnerException), ex), "Uncaught Thread Exception - " + ex.Message, MessageBoxButton.OK);
                Environment.Exit(1);
            };
        }
        private static string GetExceptionMessages(Exception e) {
            if (e == null) return string.Empty;
            if (e.InnerException != null)
                return String.Format("{0}\n{1}", e.Message, GetExceptionMessages(e.InnerException));
            return e.Message + "\n";
        }
    }
}