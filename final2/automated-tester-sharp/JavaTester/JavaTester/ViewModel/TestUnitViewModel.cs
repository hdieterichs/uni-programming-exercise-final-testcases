﻿/**
 * Author: Anton Grawitter
 * 
 * Automated Tester
 *  * NO WARRANTY! THIS IS JUST A DIRTY HACKED TEST PROGRAM.
 */
namespace JavaTester.ViewModel {
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Windows;
    using System.Windows.Input;
    using Annotations;
    using Eval;
    using Eval.Program;
    using File;
    using Utils.MicroMvvm;
    public class TestUnitViewModel : FileDependable {
        private bool _ignoreErrorStatus;
        private bool _isTestOk;
        private string _result = String.Empty;
        private TestUnit _unit;
        public TestUnitViewModel(string filePath) {
            FilePath = filePath;
        }
        public TestUnitViewModel() {
        }
        [UsedImplicitly]
        public String InputContent {
            get {
                if (_unit == null) return null;
                return _unit.Input;
            }
        }
        public string Name {
            get { return Path.GetFileNameWithoutExtension(FilePath); }
        }
        [UsedImplicitly]
        public String ExpectedOutput {
            get {
                if (_unit == null) return null;
                return _unit.Expectation.Raw;
            }
        }
        private String ExpectedResult {
            get {
                if (_unit == null) return null;
                return _unit.Expectation.ToString();
            }
        }
        private String Description {
            get {
                if (_unit == null) return null;
                return _unit.Description;
            }
        }
        [UsedImplicitly]
        public String Information {
            get {
                string output = Description ?? "No Description";
                output += "\n";
                output += "Expectation: " + ExpectedResult;
                return output.TrimStart();
            }
        }
        public String Result {
            get { return _result; }
            private set {
                if (_result == value) return;
                _result = value;
                OnPropertyChanged("Result");
            }
        }
        public bool IsTestOk {
            get { return _isTestOk; }
            set {
                if (_isTestOk == value) return;
                _isTestOk = value;
                OnPropertyChanged("IsTestOk");
            }
        }
        public bool IgnoreErrorStatus {
            get { return _ignoreErrorStatus; }
            set {
                if (value.Equals(_ignoreErrorStatus)) return;
                _ignoreErrorStatus = value;
                UpdateIsTestOk();
                OnPropertyChanged("IgnoreErrorStatus");
            }
        }
        [UsedImplicitly]
        public ICommand ShowInExplorer {
            get {
                return new RelayCommand(param => {
                    var info = new ProcessStartInfo {
                        FileName = "explorer",
                        Arguments = string.Format("/e, /select, \"{0}\"", Path.GetFullPath(FilePath))
                    };
                    Process.Start(info);
                }, param => FilePath != null && File.Exists(Path.GetFullPath(FilePath)));
            }
        }
        public bool IsFinished { get; private set; }
        private void UpdateIsTestOk() {
            if (_unit == null)
                IsTestOk = false;
            else if (!IgnoreErrorStatus && _unit.HasApplicationFailed)
                IsTestOk = false;
            else
                IsTestOk = _unit.IsSuccess;
        }
        protected override void Init(string path) {
            try {
                _unit = TestUnit.FromFile(path);
                if (_unit == null)
                    throw new InvalidDataException("Unable to parse test " + Path.GetFileNameWithoutExtension(path));
            } catch (Exception e) {
                MessageBox.Show("JavaTest could not be created " + path + ". " + e.Message);
            }
        }
        public void RunTest(TestProgram program) {
            if (program == null || _unit == null) return;
            try {
                ClearResult();
                _unit.RunTest(program);
                Result = _unit.Result.Raw;
                UpdateIsTestOk();
            } catch (Exception e) {
                Result = "Test did not finish. " + e.Message;
                IsTestOk = false;
            } finally {
                IsFinished = true;
            }
        }
        public void ClearResult() {
            IsFinished = false;
            IsTestOk = false;
        }
        public override string ToString() {
            return Name;
        }
    }
}