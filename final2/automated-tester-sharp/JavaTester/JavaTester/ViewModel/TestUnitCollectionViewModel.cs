﻿/**
 * Author: Anton Grawitter
 * 
 * Automated Tester
 *  * NO WARRANTY! THIS IS JUST A DIRTY HACKED TEST PROGRAM.
 */
namespace JavaTester.ViewModel {
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;
    using System.Windows.Input;
    using Annotations;
    using Eval.Program;
    using File;
    using Properties;
    using Utils;
    using Utils.MicroMvvm;
    using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
    public sealed class TestUnitCollectionViewModel : INotifyPropertyChanged {
        private double _progress;
        private FileDependableCollection<TestUnitViewModel> _tests;
        public TestUnitCollectionViewModel() {
            InitProgram();
            InitTestFolder();
            Progress = 1.0;
        }
        public double Progress {
            get { return _progress; }
            private set {
                if (Math.Abs(value - _progress) < Double.Epsilon) return;
                _progress = value;
                OnPropertyChanged("Progress");
            }
        }
        public FileDependableCollection<TestUnitViewModel> Tests {
            get { return _tests; }
            private set {
                if (_tests == value) return;
                _tests = value;
                OnPropertyChanged("Tests");
            }
        }
        private TestProgram Program { get; set; }
        public String ProgramPath {
            get { return Settings.Default.ProgramPath; }
            set {
                if (ProgramPath == value) return;
                Settings.Default.ProgramPath = value;
                InitProgram();
                Settings.Default.Save();
                BeginInvokeTestAll();
                OnPropertyChanged("ProgramPath");
            }
        }
        public String TestFilePath {
            get { return Settings.Default.TestFilePath; }
            set {
                if (TestFilePath == value) return;
                Settings.Default.TestFilePath = value;
                InitTestFolder();
                Settings.Default.Save();
                OnPropertyChanged("TestFilePath");
            }
        }
        public ICommand JarPathCommand {
            get {
                return new RelayCommand(o => {
                    const string filter = "Java (*.jar,*.class,*.java)|*.jar;*.class;*.java|Executable (*.exe)|*.exe";
                    var openFileDialog1 = new OpenFileDialog {Filter = filter};
                    if (openFileDialog1.ShowDialog() == true)
                        ProgramPath = openFileDialog1.FileName;
                });
            }
        }
        public ICommand TestPathCommand {
            get {
                return new RelayCommand(o => {
                    var dialog = new FolderBrowserDialog();
                    if (TestFilePath != null)
                        dialog.SelectedPath = TestFilePath;
                    DialogResult result = dialog.ShowDialog();
                    if (result == DialogResult.OK)
                        TestFilePath = dialog.SelectedPath;
                });
            }
        }
        public ICommand RunCommand {
            get {
                return new RelayCommand(o => {
                    if (!MakeSureDirectoriesAreSet()) return;
                    if (Math.Abs(Progress - 1.0) < Double.Epsilon || Math.Abs(UpdateProgress() - 1.0) < Double.Epsilon)
                        BeginInvokeTestAll();
                });
            }
        }
        public bool IgnoreExitStatus {
            get { return Settings.Default.IgnoreExitStatus; }
            set {
                if (value.Equals(Settings.Default.IgnoreExitStatus)) return;
                Settings.Default.IgnoreExitStatus = value;
                if (_tests != null)
                    _tests.ForEach(o => o.IgnoreErrorStatus = value);
                Settings.Default.Save();
                OnPropertyChanged("IgnoreExitStatus");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void InitProgram() {
            if (String.IsNullOrWhiteSpace(ProgramPath) || !File.Exists(ProgramPath))
                Program = null;
            else
                try {
                    Program = TestProgram.FromFile(ProgramPath);
                    if (Program == null)
                        throw new InvalidProgramException("Program is unsupported");
                } catch (Exception e) {
                    MessageBox.Show("Invalid Program: " + e.Message);
                    Program = null;
                }
        }
        private void InitTestFolder() {
            if (Tests != null) {
                Tests.Dispose();
                Tests = null;
            }
            if (String.IsNullOrWhiteSpace(TestFilePath) || !Directory.Exists(TestFilePath))
                TestFilePath = Directory.GetCurrentDirectory();
            if (String.IsNullOrWhiteSpace(TestFilePath) || !Directory.Exists(TestFilePath))
                return;
            Tests = new FileDependableCollection<TestUnitViewModel>(TestFilePath, "*.xml") {KeepSorted = true};
            Tests.FileAdded += (sender, args) => {
                if (!(args.File is TestUnitViewModel)) return;
                var file = args.File as TestUnitViewModel;
                file.IgnoreErrorStatus = IgnoreExitStatus;
                BeginInvokeTest(file);
            };
            BeginInvokeTestAll();
        }
        private double UpdateProgress() {
            if (Tests.Count == 0) return Progress = 1;
            return Progress = (double)Tests.Aggregate(0, (i, o) => i + (o.IsFinished ? 1 : 0)) / Tests.Count;
        }
        private void BeginInvokeTest(TestUnitViewModel model) {
            if (String.IsNullOrEmpty(ProgramPath) || String.IsNullOrEmpty(TestFilePath) || Tests == null) return;
            model.IgnoreErrorStatus = IgnoreExitStatus;
            new Action(() => model.RunTest(Program)).BeginInvoke(r => UpdateProgress(), null);
        }
        private void BeginInvokeTestAll() {
            if (Tests == null) return;
            Progress = 0;
            Tests.ForEach(o => o.ClearResult());
            UpdateProgress();
            Tests.ForEach(BeginInvokeTest);
        }
        private bool MakeSureDirectoriesAreSet() {
            if (ProgramPath == null)
                JarPathCommand.Execute(null);
            if (ProgramPath == null)
                return false;
            if (TestFilePath == null)
                TestPathCommand.Execute(null);
            return TestFilePath != null && Tests != null;
        }
        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged(string propertyName) {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}