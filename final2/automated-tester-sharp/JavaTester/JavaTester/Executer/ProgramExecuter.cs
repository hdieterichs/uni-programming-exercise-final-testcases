﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Executer {
    using System;
    using System.Diagnostics;
    using System.IO;
    public static class ProgramExecuter {
        public static String Execute(string path, params string[] args) {
            if (path == null || !File.Exists(path))
                throw new ArgumentException("Executable does not exist");
            string directory = Path.GetDirectoryName(path);
            if (directory == null || !Directory.Exists(directory))
                throw new ArgumentException("Directory not found.");
            string app = Path.GetFileNameWithoutExtension(path);
            Process proc = Process.Start(new ProcessStartInfo {
                FileName = path,
                Arguments = String.Join(" ", args),
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                WorkingDirectory = directory,
            });
            if (proc == null)
                throw new ApplicationException(app + " could not be started!");
            string output = proc.StandardOutput.ReadToEnd();
            string errorMessage = proc.StandardError.ReadToEnd();
            proc.WaitForExit();
            int exitCode = proc.ExitCode;
            proc.Close();
            if (exitCode == 0) return !String.IsNullOrWhiteSpace(output) ? output : errorMessage;
            errorMessage = !String.IsNullOrWhiteSpace(errorMessage) ? errorMessage : output;
            errorMessage = !String.IsNullOrWhiteSpace(errorMessage) ? " ErrorMessage: " + errorMessage : String.Empty;
            throw new ApplicationException(app + " closed with Exit-Code " + exitCode + "." + errorMessage);
        }
    }
}