﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Java {
    using System;
    using System.Diagnostics;
    using System.IO;
    using Executer;
    using Microsoft.Win32;
    public static class JavaExecutor {
        public static String HomePath {
            get {
                String path = null;
                string environmentPath = Environment.GetEnvironmentVariable("JAVA_HOME");
                if (!string.IsNullOrEmpty(environmentPath))
                    path = Path.GetFullPath(environmentPath);
                else {
                    const string javaKey = "SOFTWARE\\JavaSoft\\Java Development Kit\\";
                    using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(javaKey)) {
                        if (rk == null) return null;
                        string currentVersion = rk.GetValue("CurrentVersion").ToString();
                        using (RegistryKey key = rk.OpenSubKey(currentVersion)) if (key != null) path = Path.GetFullPath(key.GetValue("JavaHome").ToString());
                    }
                }
                if (path == null || !Directory.Exists(path))
                    return null;
                return path;
            }
        }
        public static String JavaPath {
            get {
                string home = HomePath;
                return home == null ? null : Path.Combine(home, "bin\\java.exe");
            }
        }
        public static String JarPath {
            get {
                string home = HomePath;
                return home == null ? null : Path.Combine(home, "bin\\jar.exe");
            }
        }
        public static String JavacPath {
            get {
                string home = HomePath;
                return home == null ? null : Path.Combine(home, "bin\\javac.exe");
            }
        }
        public static bool IsInstalled {
            get {
                try {
                    return HomePath != null;
                } catch {
                    return false;
                }
            }
        }
        public static string Version {
            get { return RunJava("-version"); }
        }
        public static void OpenUpdate() {
            Process.Start("http://www.java.com/download/");
        }
        public static string RunJava(params string[] args) {
            if (JavaPath == null)
                throw new NotSupportedException("Java: java.exe is not installed. (JRE missing?)");
            return ProgramExecuter.Execute(JavaPath, args);
        }
        public static string RunJavac(params string[] args) {
            if (JavacPath == null)
                throw new NotSupportedException("Java: javac.exe is not installed. (JDK missing?)");
            return ProgramExecuter.Execute(JavacPath, args);
        }
        public static string RunJar(params string[] args) {
            if (JarPath == null)
                throw new NotSupportedException("Java: jar.exe is not installed. (JDK missing?)");
            return ProgramExecuter.Execute(JarPath, args);
        }
    }
}