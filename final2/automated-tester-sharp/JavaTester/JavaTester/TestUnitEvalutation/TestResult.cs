﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval {
    using System;
    using System.Linq;
    public class TestResult {
        public TestResult(String data) {
            Raw = data;
            Preprocessed = PreprocessOutput(data);
        }
        public String Raw { get; private set; }
        public String Preprocessed { get; private set; }
        public bool IsFailureOutput {
            get {
                if (Raw == null || String.IsNullOrWhiteSpace(Raw)) return false;
                return Raw.Split('\n').Any(line => line.ToLower().StartsWith("error")) || !Raw.Contains("---");
            }
        }
        public bool IsNonFailureOutput {
            get {
                if (IsFailureOutput || Raw == null || String.IsNullOrWhiteSpace(Raw)) return false;
                return Raw.Contains("---");
            }
        }
        private bool Equals(TestResult other) {
            return string.Equals(Preprocessed, other.Preprocessed);
        }
        public override int GetHashCode() {
            unchecked {
                return Preprocessed != null ? Preprocessed.GetHashCode() : 0;
            }
        }
        private static string PreprocessOutput(string output) {
            if (String.IsNullOrEmpty(output))
                return output;
            string[] splitted = output.ToLower().Replace("\r", "").Split(new[] {"---"}, StringSplitOptions.None);
            if (splitted.Length != 2) return output.Trim();
            return String.Join("\n---\n", splitted.Select(o => {
                string[] lines = o.Split('\n');
                Array.Sort(lines);
                return String.Join("\n", lines).Trim();
            })).Replace(",", ".").Trim();
        }
        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is TestResult && Equals((TestResult)obj);
        }
    }
}