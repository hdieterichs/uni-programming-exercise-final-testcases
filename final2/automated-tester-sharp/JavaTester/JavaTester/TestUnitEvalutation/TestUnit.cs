﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval {
    using System;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using File;
    using Program;
    public class TestUnit {
        private readonly object _executionLock = new object();
        public TestUnit(TestExpectation expectation, String input, String description = null) {
            Input = input;
            Expectation = expectation;
            Description = description ?? "";
        }
        public TestExpectation Expectation { get; private set; }
        public TestResult Result { get; set; }
        public bool IsSuccess {
            get {
                if (Expectation == null)
                    return false;
                if (Expectation.ShouldMatch && !Equals(Expectation, Result))
                    return false;
                if (Result == null)
                    return false;
                if (Expectation.ShoulFail && !Result.IsFailureOutput)
                    return false;
                if (Expectation.ShouldNotFail && !Result.IsNonFailureOutput)
                    return false;
                return true;
            }
        }
        public bool HasApplicationFailed { get; private set; }
        public string Input { get; set; }
        public string Description { get; set; }
        public bool HasRun {
            get { return Result != null; }
        }
        public static TestUnit FromFile(String xmlPath) {
            Test xml;
            using (Stream reader = new FileStream(xmlPath, FileMode.Open)) {
                var serializer = new XmlSerializer(typeof(Test));
                xml = serializer.Deserialize(reader) as Test;
            }
            if (xml == null)
                return null;
            if (xml.InputFileContent == null) {
                string txtPath = xmlPath.Substring(0, xmlPath.Length - 4) + ".txt";
                xml.InputFileContent = File.Exists(txtPath) ? File.ReadAllText(txtPath) : String.Empty;
            }
            bool shouldFail = xml.ExpectedResult == "Error";
            bool shouldNotFail = xml.ExpectedResult == "Successful";
            var expectation = new TestExpectation(xml.OutputShouldBeEqualTo != null ? xml.OutputShouldBeEqualTo.Trim() : null, shouldFail, shouldNotFail);
            return new TestUnit(expectation, xml.InputFileContent.Trim(), xml.Description != null ? xml.Description.Trim() : null);
        }
        public void ClearResult() {
            lock(_executionLock) {
                Result = null;
            }
        }
        public bool RunTest(TestProgram program) {
            ClearResult();
            lock(_executionLock) {
                using (var temp = new TemporaryFile(Guid.NewGuid() + ".txt")) {
                    temp.Data = Encoding.UTF8.GetBytes(Input);
                    string result;
                    try {
                        result = program.Execute(temp.FilePath);
                        HasApplicationFailed = false;
                    } catch (ApplicationException e) {
                        result = e.Message;
                        HasApplicationFailed = true;
                    }
                    Result = new TestResult(result);
                    return IsSuccess;
                }
            }
        }
    }
}