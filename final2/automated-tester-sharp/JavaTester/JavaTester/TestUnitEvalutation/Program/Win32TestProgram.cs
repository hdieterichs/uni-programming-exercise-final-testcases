﻿/**
 * Author: Anton Grawitter
 * 
 * -untested-
 */
namespace JavaTester.Eval.Program {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Executer;
    public class Win32TestProgram : TestProgram {
        public Win32TestProgram(string filePath) : base(filePath) {
            if (String.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
                throw new FileNotFoundException("Executable " + filePath + " does not exist!");
        }
        public override string Execute(params string[] args) {
            List<string> argList = args.Select(o => "\"" + o + "\"").ToList();
            return ProgramExecuter.Execute(FilePath, argList.ToArray());
        }
    }
}