﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval.Program {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Java;
    public class JarTestProgram : TestProgram {
        public JarTestProgram(string filePath) : base(filePath) {
            if (String.IsNullOrWhiteSpace(filePath) || !filePath.ToLower().EndsWith(".jar") || !File.Exists(filePath)) throw new FileNotFoundException("File " + filePath + "is invalid");
        }
        public override string Execute(params string[] args) {
            List<string> argList = args.Select(o => "\"" + o + "\"").ToList();
            argList.Insert(0, "\"" + FilePath + "\"");
            argList.Insert(0, "-jar");
            return JavaExecutor.RunJava(argList.ToArray());
        }
    }
}