﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval.Program {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using Java;
    public sealed class JavaClassTestProgram : TestProgram {
        private const UInt32 Magic = 0xBEBAFECA; //CAFEBABE
        private static readonly Regex RegexPackage1 = new Regex(@"this(.*?)L(.*?);", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);
        private static readonly Regex RegexPackage2 = new Regex(@"L(.*?);", RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Compiled);
        private readonly string _classPath;
        private readonly string _packagePath;
        public JavaClassTestProgram(string filePath) : base(filePath) {
            if (String.IsNullOrWhiteSpace(filePath) || !filePath.ToLower().EndsWith(".class") || !File.Exists(filePath)) throw new FileNotFoundException("File " + filePath + "is invalid");
            try {
                _packagePath = ParsePackagePath(filePath);
                _classPath = GetClassPathFromPackagePath(_packagePath, filePath);
            } catch (Exception e) {
                throw new InvalidProgramException("Unable to open program (This file is not supported)", e);
            }
        }
        private static string ParsePackagePath(string fileName) {
            byte[] data = File.ReadAllBytes(fileName);
            if (data.Length < 4 || Magic != BitConverter.ToUInt32(data, 0))
                throw new InvalidProgramException("Class file invalid. Magic value mismatch");
            string s = Encoding.ASCII.GetString(data).Replace("\0", ""); //seems like c# regex cant handle \0 properly
            Match match = RegexPackage1.Match(s);
            if (!match.Success) return String.Empty;
            match = RegexPackage2.Match(match.Value);
            if (!match.Success) return String.Empty;
            int index = match.Value.LastIndexOf('/');
            return index == -1 ? String.Empty : match.Value.Substring(1, index).Replace('/', '.');
        }
        //assuming the package path equals the folder structure
        private static string GetClassPathFromPackagePath(string packagePath, string filePath) {
            string path = Path.GetDirectoryName(filePath);
            if (path == null)
                throw new FileNotFoundException();
            foreach (string package in packagePath.Split('.').Reverse().Select(o => o.ToLower())) {
                if (String.IsNullOrWhiteSpace(package))
                    continue;
                string pathName = Path.GetFileName(path);
                if (pathName != null)
                    pathName = pathName.ToLower();
                if (pathName == package)
                    path = Path.GetDirectoryName(path);
                else
                    break;
            }
            return path;
        }
        //assuming the class name equals the file name
        public override string Execute(params string[] args) {
            List<string> argList = args.Select(o => "\"" + o + "\"").ToList();
            argList.Insert(0, _packagePath + Path.GetFileNameWithoutExtension(FilePath));
            argList.Insert(0, "-classpath \"" + _classPath + "\"");
            return JavaExecutor.RunJava(argList.ToArray());
        }
    }
}