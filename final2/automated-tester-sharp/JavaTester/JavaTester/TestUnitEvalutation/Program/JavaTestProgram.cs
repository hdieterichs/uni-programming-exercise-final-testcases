﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval.Program {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using File;
    using Java;
    public sealed class JavaTestProgram : TestProgram {
        private readonly string _classPath;
        private readonly object _compilationLock = new object();
        private readonly string _packagePath;
        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();
        private TemporaryFolder _compilationFolder;
        private bool _needsRecompilation;
        public JavaTestProgram(string filePath) : base(filePath) {
            if (String.IsNullOrWhiteSpace(filePath) || !filePath.ToLower().EndsWith(".java") || !File.Exists(filePath)) throw new FileNotFoundException("File " + filePath + "is invalid");
            try {
                _packagePath = ParsePackagePath(filePath);
                _classPath = GetClassPathFromPackagePath(_packagePath, filePath);
            } catch (Exception e) {
                throw new InvalidProgramException("Unable to open program (This file is not supported)", e);
            }
            _compilationFolder = CompileToTemporaryFolder();
            _watcher.Path = _classPath;
            _watcher.Filter = "*.java";
            _watcher.IncludeSubdirectories = true;
            _watcher.EnableRaisingEvents = true;
            _watcher.Changed += OnFileChanged;
            _watcher.Created += OnFileChanged;
            _watcher.Deleted += OnFileChanged;
            _watcher.Renamed += OnFileChanged;
        }
        private static string ParsePackagePath(string fileName) {
            string f = Path.GetFileNameWithoutExtension(fileName);
            if (f == null) return "";
            const string package = "package ";
            foreach (string line in File.ReadLines(fileName).Select(line => line.Trim().ToLower())
                .Where(line => line.StartsWith(package) && line.EndsWith(";"))) return line.Substring(package.Length, line.Length - package.Length - 1).Trim() + ".";
            return "";
        }
        private void OnFileChanged(object source, FileSystemEventArgs e) {
            _needsRecompilation = true;
        }
        private static string GetClassPathFromPackagePath(string packagePath, string filePath) {
            string path = Path.GetDirectoryName(filePath);
            if (path == null)
                throw new FileNotFoundException();
            foreach (string package in packagePath.Split('.').Reverse().Select(o => o.ToLower())) {
                if (String.IsNullOrWhiteSpace(package))
                    continue;
                string pathName = Path.GetFileName(path);
                if (pathName != null)
                    pathName = pathName.ToLower();
                if (pathName == package)
                    path = Path.GetDirectoryName(path);
                else
                    break;
            }
            return path;
        }
        private TemporaryFolder CompileToTemporaryFolder() {
            var folder = new TemporaryFolder();
            List<string> files = Directory.GetFiles(_classPath, "*.java", SearchOption.AllDirectories)
                .Select(s => "\"" + Path.GetFullPath(s) + "\"").ToList();
            files.Insert(0, "-d \"" + Path.GetFullPath(folder.DirectoryPath) + "\"");
            JavaExecutor.RunJavac(files.ToArray());
            _needsRecompilation = false;
            return folder;
        }
        public override string Execute(params string[] args) {
            lock(_compilationLock) {
                //you dont wanna compile it multiple times accidently
                if (_needsRecompilation)
                    _compilationFolder = CompileToTemporaryFolder();
            }
            List<string> argList = args.Select(o => "\"" + o + "\"").ToList();
            argList.Insert(0, _packagePath + Path.GetFileNameWithoutExtension(FilePath));
            argList.Insert(0, "-classpath \"" + _compilationFolder.DirectoryPath + "\"");
            return JavaExecutor.RunJava(argList.ToArray());
        }
    }
}