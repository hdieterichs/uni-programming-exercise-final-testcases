﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval.Program {
    using System;
    using System.IO;
    using Annotations;
    public abstract class TestProgram {
        [NotNull] private readonly String _filePath = String.Empty;
        protected TestProgram(String filePath) {
            _filePath = filePath;
        }
        [NotNull]
        public String FilePath {
            get { return _filePath; }
        }
        public abstract string Execute(params string[] args);
        public static TestProgram FromFile(string programPath) {
            if (String.IsNullOrWhiteSpace(programPath) || !File.Exists(programPath))
                throw new FileNotFoundException(programPath + " does not exist.");
            string programPathLower = programPath.ToLower();
            if (programPathLower.EndsWith(".jar"))
                return new JarTestProgram(programPath);
            if (programPathLower.EndsWith(".class"))
                return new JavaClassTestProgram(programPath);
            if (programPathLower.EndsWith(".java"))
                return new JavaTestProgram(programPath);
            if (programPathLower.EndsWith(".exe"))
                return new Win32TestProgram(programPath);
            return null;
        }
    }
}