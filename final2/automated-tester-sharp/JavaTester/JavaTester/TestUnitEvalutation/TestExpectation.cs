﻿/**
 * Author: Anton Grawitter
 */
namespace JavaTester.Eval {
    using System.Diagnostics;
    public class TestExpectation : TestResult {
        public TestExpectation(string expectedOutput, bool shouldFail, bool shouldNotFail) : base(expectedOutput) {
            Debug.Assert(!(shouldFail && shouldNotFail), "Test settings invalid. Unable to fail and not fail at the same time");
            ShoulFail = shouldFail;
            ShouldMatch = shouldNotFail && expectedOutput != null;
            ShouldNotFail = shouldNotFail;
        }
        public bool ShoulFail { get; set; }
        public bool ShouldMatch { get; set; }
        public bool ShouldNotFail { get; set; }
        public override string ToString() {
            string output = "";
            if (ShoulFail)
                output += "Error";
            else if (ShouldNotFail)
                output += "Successful";
            else
                output += "Successful or Error";
            if (ShouldMatch)
                output += " and Match";
            return output;
        }
    }
}