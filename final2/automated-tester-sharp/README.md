Automatisches Test-Tool mit GUI (written in c#/.net)
====================================================

Der automatische Tester testet ein angegebenes Programm mit allen Testfällen in einem angegebenen Ordner.
Die Testfälle befinden sich unter final2/input-files.

Benutzung
---------
Starte das Programm aus final2/automated-tester-sharp/JavaTester/JavaTester/bin/Release/JavaTester.exe, drücke den Button "Run" und folge den Anweisungen.