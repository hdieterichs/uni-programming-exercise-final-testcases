Eingabe-Dateien / Testfälle
===========================

Damit kein heilloses Chaos entsteht, gibt es ein paar Richtlinien beim Verfassen von Eingabe-Dateien bzw. von Testfällen.

Jeder Testfall wird durch eine Xml-Datei beschrieben. Diese Xml-Datei sieht wie folgt aus:
``` xml
<Test ExpectedResult="Successful">
    <Description>
        Eine kurze Beschreibung des Tests.
        Was wird getestet? Warum könnte er schiefgehen? etc.
    </Description>
    <InputFileContent>
musik/happy.mp3,audio,30,genre=funk,author=Pharrell Williams,fun
dokumente/Abschlussaufgabe 1,program,5,author=me
[...]
bilder/Oma.jpg,image,1,family
    </InputFileContent>
    <OutputShouldBeEqualTo>
/AudioGenre=1,00
[...]
/author=me/fun=undefined/executable=0,99
---
/author=Pharrell Williams/"musik/happy.mp3"
[...]
/author=undefined/"bilder/Oma.jpg"
    </OutputShouldBeEqualTo>
</Test>
```
`ExpectedResult` kann `Successful`, `Error` oder `SuccessfulOrError` sein.
`Successful` bedeutet, dass das Programm die Eingabedatei korrekt verarbeiten können muss, `Error` bedeutet, dass das Programm eine Fehlermeldung ausgeben muss. `SuccessfulOrError` heißt, dass das Ergebnis der Ausführung von der Interpretation der Aufgabenstellung abhängt.
Dies bedeutet, dass der automatische Tester `SuccessfulOrError`-Tests immer erfolgreich durchläuft.

Das `OutputShouldBeEqualTo`-Element ist optional, bei `Error`-Tests sollte es weggelassen werden, da die Fehlermeldungen nicht einheitlich sind.
Das `InputFileContent` ist auch optional, wenn es aber nicht angegeben wird, muss stattdessen eine Datei angelegt werden, die genau so wie die Xml-Datei heißt, jedoch mit ".txt" endet. Zu "test1.xml" gehört also die Eingabe-Datei "test1.txt".
Allerdings wird empfohlen, stattdessen das `InputFileContent`-Element zu nutzen, da der Test-Fall dann übersichtlicher ist - schließlich besteht er dann nur noch aus einer Datei.
Das automatische Test-Tool kann diese integrierten Eingabe-Dateien wieder extrahieren, sodass sie direkt dem Programm übergeben werden können. Auf Wunsch kann das Test-Tool die Tests direkt ausführen oder nur die Eingabe-Dateien in einen angegebenen Ordner extrahieren.