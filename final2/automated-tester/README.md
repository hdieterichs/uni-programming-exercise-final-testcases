Automatisches Test-Tool
=======================

Der automatische Tester testet ein angegebenes Programm mit allen Testfällen in einem angegebenen Ordner.
Die Testfälle befinden sich unter final2/input-files.

Benutzung
---------
Der automatische Tester kann direkt über die jar-Datei im dist-Ordner gestartet werden.
Das Programm, welches er testen soll, und der Ordner, indem die Test-Dateien liegen, kann per Kommandozeile oder über eine Konfigurationsdatei angegeben werden. Die Konfigurationsdatei muss "config.properties" heißen und entweder im aktuellen Arbeitsverzeichnis oder im dist-Verzeichnis, also dem Ordner der jar-Datei liegen.
Wird ein benötigtes Kommandozeilen-Argument nicht angegeben, wird, falls vorhanden, in einer dieser Konfigurationsdateien nachgeschaut.

Beispielaufruf ohne Konfigurationsdatei
---------------------------------------
```
java -jar dist/JavaApplicationFinal2Tester.jar -p "java -cp C:\Use[...]\build\classes final2.de.hediet.Program %s" -t "..\input-files"
```
Das Verzeichnis zum eigenen Programm muss natürlich noch angepasst werden.

Beispielaufruf mit Konfigurationsdatei
--------------------------------------
```
java -jar dist/JavaApplicationFinal2Tester.jar
```

Wobei die Konfigurationsdatei wie folgt aussehen kann:
```
program=java -cp C:\\Users\\Hen[...]\\Task2FileSystem\\build\\classes final2.de.hediet.Program %s

# This property specifies the directory of the input files.
testcasedir=..\\input-files
```
Wichtig: Bachslashes müssen in der *Konfigurationsdatei* escaped werden! Aus `C:\Users\Hen[...]` wird also `C:\\Users\\Hen[...]`.
Werden die Argumente allerdings als Kommandozeilen-Paramater übergeben, dürfen sie unter Windows nicht escaped werden.
Wie es unter Linux aussieht, weiß ich nicht. Im Notfall einfach beides ausprobieren.