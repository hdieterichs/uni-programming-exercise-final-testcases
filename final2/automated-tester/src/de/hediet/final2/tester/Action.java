package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
public enum Action {
    TEST, EXTRACT_INPUTFILES
}
