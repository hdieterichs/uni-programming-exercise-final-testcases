package de.hediet.final2.tester;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Henning Dieterichs
 */
public class TestRunner {

    private final Configuration configuration;

    public TestRunner(Configuration configuration) {
        this.configuration = configuration;
    }

    public TestResult runTest(TestCase test) throws TestCaseRunException {
        try {
            Runtime rt = Runtime.getRuntime();
            Process p = rt.exec(String.format(configuration.getProgramToTestCommandLine(),
                    test.getInputFile()));

            String error = readStream(p.getErrorStream(), 1024);
            String output = readStream(p.getInputStream(), 1024);
            p.waitFor();

            String totalOutput = output + System.lineSeparator() + error;

            //assuming there is no other "---" sequence in output
            boolean successful = output.contains("---") && !totalOutput.toLowerCase().contains("error");

            if (successful) {

                if (test.getExpectedTestResult() == ExpectedTestResult.ERROR) {
                    return new TestResult(false, totalOutput, test,
                            "The program should break and give an error message.");
                }

                if (test.getExpectedOutput() != null && !canonizeOutput(output).equals(canonizeOutput(test.getExpectedOutput()))) {
                    canonizeOutput(output).equals(canonizeOutput(test.getExpectedOutput()));
                      return new TestResult(false, totalOutput, test,
                            "The expected output is different.");
                }
                
                
            } else if (test.getExpectedTestResult() == ExpectedTestResult.SUCCESSFUL) {
                return new TestResult(false, totalOutput, test, "The program should accept this input file.");
            }

            return new TestResult(true, totalOutput, test, "Successful.");

        } catch (InterruptedException | IOException ex) {
            throw new TestCaseRunException("An exception occured while executing test '"
                    + test.getName() + "'", ex);
        }
    }

    private static String canonizeOutput(String output) {
        //to be exact, tag values are case sensitive, but that would be too hard to parse.
        String str = output.replaceAll("\r\n", "\n").replaceAll("\\.", ",").toLowerCase();
        String[] parts = str.trim().split("---");
        String result = "";
        if (parts.length > 0)
            result += canonizeList(parts[0]);
        result += "\n---\n";
        if (parts.length > 1)
            result += canonizeList(parts[1]);
        return result;
    }

    private static String canonizeList(String str) {
        String[] parts = str.trim().split("\n");
        List<String> partList = Arrays.asList(parts);

        Collections.sort(partList);

        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (String item : partList) {
            if (isFirst) {
                isFirst = false;
            } else {
                sb.append("\n");
            }
            sb.append(item);
        }
        return sb.toString();
    }

    private static String readStream(final InputStream is, final int bufferSize) {
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            final Reader in = new InputStreamReader(is, "UTF-8");
            try {
                for (;;) {
                    int rsz = in.read(buffer, 0, buffer.length);
                    if (rsz < 0) {
                        break;
                    }
                    out.append(buffer, 0, rsz);
                }
            } finally {
                in.close();
            }
        } catch (UnsupportedEncodingException ex) {
            /* ... */
        } catch (IOException ex) {
            /* ... */
        }
        return out.toString();
    }
}
