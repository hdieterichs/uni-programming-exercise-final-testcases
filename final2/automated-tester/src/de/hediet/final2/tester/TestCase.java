package de.hediet.final2.tester;

import java.io.File;

/**
 *
 * @author Henning Dieterichs
 */
public abstract class TestCase {
    
    public abstract String getName();
    
    public abstract String getDescription();
    
    public abstract File getInputFile();
    
    public abstract String getInput();
    
    public abstract ExpectedTestResult getExpectedTestResult();
    
    public abstract String getExpectedOutput();
}
