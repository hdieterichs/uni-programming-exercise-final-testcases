package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
public class TestCaseLoadException extends Exception {
    public TestCaseLoadException(String message) {
        super(message);
    }
    
    public TestCaseLoadException(String message, Throwable cause) {
        super(message, cause);
    }

}
