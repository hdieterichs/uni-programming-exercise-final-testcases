package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
public class TestResult {
    
    private final boolean isSuccessful;
    private final String programOutput;
    private final TestCase testCase;
    private final String message;
    
        
    public TestResult(boolean isSuccessful, String programOutput, TestCase testCase, String message) {
        this.isSuccessful = isSuccessful;
        this.programOutput = programOutput;
        this.testCase = testCase;
        this.message = message;
        
        
    }

    public String getMessage() {
        return message;
    }
    
    public boolean isSuccessful() {
        return isSuccessful;
    }

    public String getProgramOutput() {
        return programOutput;
    }

    public TestCase getTestCase() {
        return testCase;
    }
}
