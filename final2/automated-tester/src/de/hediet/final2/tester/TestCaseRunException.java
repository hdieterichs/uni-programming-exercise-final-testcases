package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
public class TestCaseRunException extends Exception {

    public TestCaseRunException(String message, Throwable cause) {
        super(message, cause);
    }
}
