package de.hediet.final2.tester;

import com.martiansoftware.jsap.JSAPException;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;

/**
 * The program.
 *
 * @author Henning Dieterichs
 * @version 0.1
 */
public class Program {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            Configuration config = new Configuration(args);

            Collection<TestCase> testCases = loadTestCases(config);

            if (config.getAction() == Action.TEST) {
                runTests(config, testCases);
            } else if (config.getAction() == Action.EXTRACT_INPUTFILES) {
                extractInputFiles(config, testCases);
            }

        } catch (IOException | TestCaseLoadException | TestCaseRunException | JSAPException ex) {
            System.err.println("An exception occurred: " + ex.toString());
        }
    }

    private static void extractInputFiles(Configuration config,
            Collection<TestCase> testCases) throws IOException {
        for (TestCase testCase : testCases) {
            Files.copy(testCase.getInputFile().toPath(), new File(config.getExtractedInputFileOutputDir() + 
                    testCase.getName() + ".txt").toPath());
        }
    }

    private static void runTests(Configuration config,
            Collection<TestCase> testCases) throws TestCaseRunException {
        TestRunner runner = new TestRunner(config);

        int testCount = 0;
        int failedTests = 0;
        
        for (TestCase testCase : testCases) {
            testCount++;
            
            TestResult result = runner.runTest(testCase);

            PrintStream ps;
            if (result.isSuccessful()) {
                ps = System.out;
            } else {
                ps = System.err;
            }

            ps.println(String.format("Test '%s':", testCase.getName()));
            ps.println("------------------------");

            ps.println("Test result: " + result.getMessage());

            if (!result.isSuccessful()) {
                failedTests++;
                
                if (testCase.getDescription() != null) {
                    ps.println("Description:");
                    ps.println(testCase.getDescription().trim());
                }

                ps.println();
                ps.println("Input:");
                ps.println(testCase.getInput());

                if (testCase.getExpectedOutput() != null) {
                    ps.println();
                    ps.println("Expected output:");
                    ps.println(testCase.getExpectedOutput().trim());
                }
                if (result.getProgramOutput() != null) {
                    ps.println();
                    ps.println("Actual output:");
                    ps.println(result.getProgramOutput().trim());
                }
            }
            else if (testCase.getExpectedOutput() == null) {
                if (testCase.getDescription() != null) {
                    ps.println("Description:");
                    ps.println(testCase.getDescription().trim());
                }
                if (result.getProgramOutput() != null) {
                    ps.println();
                    ps.println("Actual output:");
                    ps.println(result.getProgramOutput().trim());
                }
            }



            ps.println("------------------------");

            ps.println();
            ps.println();
        }
        
        System.out.println("Successfully passed tests: " + (testCount - failedTests) + " of " + testCount);
    }

    private static Collection<TestCase> loadTestCases(Configuration config) throws TestCaseLoadException {

        if (!config.getTestCasesInputFileDir().exists()) {
            throw new TestCaseLoadException("Directory does not exist.");
        }

        File[] fileList = config.getTestCasesInputFileDir().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".xml");
            }
        });

        Collection<TestCase> result = new ArrayList<>();

        for (File f : fileList) {
            result.add(new XmlTestCase(f));
        }

        return result;
    }
}
