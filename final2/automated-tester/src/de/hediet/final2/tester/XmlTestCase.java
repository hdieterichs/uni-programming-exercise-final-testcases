package de.hediet.final2.tester;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XmlTestCase extends TestCase {

    private final String name;
    private final String description;
    private final ExpectedTestResult expectedTestResult;
    private File inputFile;
    private String inputFileContent;
    private final String expectedOutput;

    public XmlTestCase(File xmlFile) throws TestCaseLoadException {
        name = removeEnd(xmlFile.getName(), 4);
        inputFile = new File(removeEnd(xmlFile.getAbsolutePath(), 4) + ".txt");

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(xmlFile);

            Node testNode = doc.getDocumentElement();
            String expectedResultStr = trimIfNotNull(getStringValueFromAttribute(testNode, "ExpectedResult"));
            expectedTestResult = ExpectedTestResult.parse(expectedResultStr);

            description = trimIfNotNull(getStringValueFromChild(testNode, "Description"));
            
            expectedOutput = trimIfNotNull(getStringValueFromChild(testNode, "OutputShouldBeEqualTo"));

            if (!inputFile.exists()) {
                inputFileContent = getStringValueFromChild(testNode, "InputFileContent");
                if (inputFileContent == null) {
                    throw new TestCaseLoadException("Cannot create xml test case from file '"
                            + xmlFile.getPath() + "', because no input file exists.");
                }
                else
                    inputFileContent = inputFileContent.trim();
                
                //temp files will be deleted automatically
                inputFile = File.createTempFile("temp-input-file", Long.toString(System.nanoTime()));

                try (PrintWriter out = new PrintWriter(inputFile)) {
                    out.write(inputFileContent);
                }
            }

        } catch (IOException | ParserConfigurationException | SAXException | TextParseException ex) {
            throw new TestCaseLoadException("Cannot create xml test case from file '"
                    + xmlFile.getPath() + "'", ex);
        }
    }
    
    private static String trimIfNotNull(String str) {
        if (str == null)
            return null;
        return str.trim();
    }
            

    private static String removeEnd(String str, int count) {
        return str.substring(0, str.length() - count);
    }

    private static String getStringValueFromAttribute(Node node, String name) {
        return node.getAttributes().getNamedItem(name).getTextContent();
    }

    private static String getStringValueFromChild(Node node, String name) {

        Node subChild = null;

        for (int i = 0; i < node.getChildNodes().getLength(); i++) {
            Node n = node.getChildNodes().item(i);

            if (n.getNodeName().equals(name)) {
                if (subChild != null) {
                    throw new RuntimeException("Element defined twice");
                }
                subChild = n;
            }
        }

        if (subChild == null) {
            return null;
        }

        return subChild.getTextContent();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public File getInputFile() {
        return inputFile;
    }

    @Override
    public String getInput() {
        return inputFileContent;
    }

    @Override
    public ExpectedTestResult getExpectedTestResult() {
        return expectedTestResult;
    }

    @Override
    public String getExpectedOutput() {
        return expectedOutput;
    }

}
