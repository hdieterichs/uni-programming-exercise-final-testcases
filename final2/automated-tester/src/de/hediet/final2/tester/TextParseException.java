package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
class TextParseException extends Exception {

    public TextParseException(String message) {
        super(message);
    }
    
}
