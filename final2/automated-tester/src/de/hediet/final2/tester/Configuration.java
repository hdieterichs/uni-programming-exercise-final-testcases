package de.hediet.final2.tester;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.defaultsources.PropertyDefaultSource;
import java.io.File;

/**
 *
 * @author Henning Dieterichs
 */
public class Configuration {

    private final String programToTestCommandLine;
    private final File testCasesInputFileDir;
    private final String extractedInputFileOutputDir;
    private final Action action;

    public Configuration(String[] commandLineArguments) throws JSAPException {

        JSAP jsap = new JSAP();

        jsap.registerDefaultSource(new PropertyDefaultSource("config.properties", false));
        jsap.registerDefaultSource(new PropertyDefaultSource(
                new File(Configuration.class.getProtectionDomain()
                        .getCodeSource().getLocation().getPath()).getParent()
                + "/config.properties", false));
        
        jsap.registerParameter(new FlaggedOption("action")
                .setStringParser(JSAP.STRING_PARSER)
                .setDefault("test")
                .setRequired(true)
                .setShortFlag('a')
                .setLongFlag("action")
                .setHelp("The action which can be 'test' "
                        + "or 'extract-input-files'. Default is 'test'"));

        jsap.registerParameter(new FlaggedOption("program")
                .setStringParser(JSAP.STRING_PARSER)
                .setRequired(true)
                .setShortFlag('p')
                .setLongFlag("program")
                .setHelp("The program command line which will be tested, e.g. "
                        + "'java -cp \\build\\classes' final2.de.hediet.Program %s'. "
                        + "%s will be replaced with the actual input file."));

        jsap.registerParameter(new FlaggedOption("testcasedir")
                .setStringParser(JSAP.STRING_PARSER)
                .setDefault("../input-files")
                .setRequired(true)
                .setShortFlag('t')
                .setLongFlag("testcasedir")
                .setHelp("The directory which contains the xml files."));

        jsap.registerParameter(new FlaggedOption("outputdir")
                .setStringParser(JSAP.STRING_PARSER)
                .setDefault("")
                .setRequired(true)
                .setShortFlag('o')
                .setLongFlag("outputdir")
                .setHelp("The directory the extracted input file will be saved to."));

        JSAPResult config = jsap.parse(commandLineArguments);
        if (!config.success()) {
            System.err.println();
            System.err.println("Usage: java "
                    + Program.class.getName());
            System.err.println("                "
                    + jsap.getUsage());
            System.err.println();
            System.exit(1);
        }

        String actionStr = config.getString("action");
        switch (actionStr) {
            case "test":
                action = Action.TEST;
                break;
            case "extract-input-files":
                action = Action.EXTRACT_INPUTFILES;
                break;
            default:
                throw new JSAPException("Action '" + actionStr
                        + "' is not supported. Valid actions are 'test' and 'extract-input-files'");
        }

        programToTestCommandLine = config.getString("program");
        testCasesInputFileDir = new File(config.getString("testcasedir"));
        String outputDir = config.getString("outputdir");
        if (!outputDir.isEmpty() && !outputDir.endsWith("/")) {
            extractedInputFileOutputDir = outputDir + "/";
        } else {
            extractedInputFileOutputDir = outputDir;
        }
    }

    public String getProgramToTestCommandLine() {
        return programToTestCommandLine;
    }

    public File getTestCasesInputFileDir() {
        return testCasesInputFileDir;
    }

    public String getExtractedInputFileOutputDir() {
        return extractedInputFileOutputDir;
    }

    public Action getAction() {
        return action;
    }
}
