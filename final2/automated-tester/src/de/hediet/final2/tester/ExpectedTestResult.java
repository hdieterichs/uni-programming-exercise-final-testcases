package de.hediet.final2.tester;

/**
 *
 * @author Henning Dieterichs
 */
public enum ExpectedTestResult {

    SUCCESSFUL, ERROR, SUCCESSFULL_OR_ERROR;

    public static ExpectedTestResult parse(String str) throws TextParseException {
        if (str != null) {
            switch (str) {
                case "Successful":
                    return SUCCESSFUL;
                case "Error":
                    return ERROR;
                case "SuccessfulOrError":
                    return SUCCESSFULL_OR_ERROR;
            }
        }

        throw new TextParseException("Cannot parse str");
    }
}
