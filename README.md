Dieses Repository enthält insbesondere Testfälle für die zweite Programmieren-Abschlussaufgabe.

Im Ordner final2/input-files befinden sich Eingabedateien mit erwartetem Ergebnis.

Im Ordner final2/automated-tester befindet sich ein Test-Programm, mit dem ein angegebenes Programm mit allen Test-Dateien durchgetestet werden kann.

Im Ordner final2/automated-tester-sharp befindet sich ein .Net Test-Programm mit GUI, mit dem ein angegebenes Programm mit allen Test-Dateien durchgetestet werden kann.

Für mehr Informationen siehe die Readme-Datei im entsprechenden Ordner.
Je mehr Werbung über dieses Repository gemacht wird, desto besser kann getestet werden!

Fühlt euch frei, eigene Test-Dateien per Pull Request in dieses Repository einzufügen (Wer nicht weiß, wie das geht, oder kein Bitbucket-Account hat, kann die Tests auch jemandem zukommen lassen, der das kann). Gleiches gilt natürlich auch für gefundene Fehler in den Test-Dateien oder im Test-Tool.

Ansonsten noch viel Spaß beim Testen!

WICHTIG: KEINE HINWEISE ODER HILFESTELLUNG ZUR LÖSUNG COMMITTEN!